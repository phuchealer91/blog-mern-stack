"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.forgotPassword = exports.refreshToken = exports.logout = exports.facebookLogin = exports.googleLogin = exports.login = exports.activeAccount = exports.register = void 0;
const bcrypt_1 = __importDefault(require("bcrypt"));
const google_auth_library_1 = require("google-auth-library");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const node_fetch_1 = __importDefault(require("node-fetch"));
const serverMail_1 = __importDefault(require("../config/serverMail"));
const valid_1 = require("../middleware/valid");
const user_model_1 = __importDefault(require("../models/user.model"));
const generateToken_1 = require("./../config/generateToken");
const client = new google_auth_library_1.OAuth2Client(`${process.env.MAIL_CLIENT_ID}`);
const CLIENT_URL = `${process.env.BASE_URL}`;
const register = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { name, account, password } = req.body;
        const user = yield user_model_1.default.findOne({ account });
        if (user)
            return res
                .status(400)
                .json({ msg: 'Email or Phone number already exists.' });
        const passwordHash = yield bcrypt_1.default.hash(password, 12);
        const newUser = { name, account, password: passwordHash };
        const active_token = (0, generateToken_1.generateActiveToken)({ newUser });
        const url = `${CLIENT_URL}/active/${active_token}`;
        if ((0, valid_1.validateEmail)(account)) {
            (0, serverMail_1.default)(account, url, 'Verify your email address');
            return res.status(200).json({ msg: 'Success! Please check your email.' });
        }
        // return res.status(201).json({
        //   status: 'OK',
        //   msg: 'Register successfully.',
        //   data: newUser,
        //   active_token,
        // })
    }
    catch (error) {
        return res.status(500).json('Server error');
    }
});
exports.register = register;
const activeAccount = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { active_token } = req.body;
        const decoded = (jsonwebtoken_1.default.verify(active_token, `${process.env.ACTIVE_TOKEN_SECRET}`));
        const { newUser } = decoded;
        if (!newUser)
            return res.status(400).json({ msg: 'Invalid authentication' });
        const user = yield user_model_1.default.findOne({ account: newUser.account });
        if (user)
            return res.status(400).json({ msg: 'Account already exists.' });
        const new_user = new user_model_1.default(newUser);
        yield new_user.save();
        return res.status(201).json({ msg: 'Account has been activated!' });
    }
    catch (error) {
        let errMsg = error === null || error === void 0 ? void 0 : error.message;
        if (error.code === 11000) {
            errMsg = Object.keys(error.keyValue)[0] + 'already exists.';
        }
        return res.status(500).json({ msg: errMsg });
    }
});
exports.activeAccount = activeAccount;
const login = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { account, password } = req.body;
        const user = yield user_model_1.default.findOne({ account });
        if (!user)
            return res.status(400).json({ msg: 'Not found' });
        // if user exists
        loginUser(user, password, res);
    }
    catch (error) {
        return res.status(500).json('Server error');
    }
});
exports.login = login;
const googleLogin = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { id_token } = req.body;
        // console.log('id_token', id_token)
        const verify = yield client.verifyIdToken({
            idToken: id_token,
            audience: `${process.env.MAIL_CLIENT_ID}`,
        });
        const { email, email_verified, name, picture } = (verify.getPayload());
        if (!email_verified)
            return res.status(500).json({ msg: 'Email verification failed.' });
        const password = email + 'your google secrect password';
        const passwordHash = yield bcrypt_1.default.hash(password, 12);
        const user = yield user_model_1.default.findOne({ account: email });
        if (user) {
            loginUser(user, password, res);
        }
        else {
            const user = {
                name,
                account: email,
                password: passwordHash,
                avatar: picture,
                type: 'google',
            };
            registerUser(user, res);
        }
    }
    catch (error) {
        return res.status(500).json('Server error');
    }
});
exports.googleLogin = googleLogin;
const facebookLogin = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { accessToken, userID } = req.body;
        const URL = `https://graph.facebook.com/v14.0/${userID}/?fields=id,first_name,last_name,middle_name,email,picture&access_token=${accessToken}`;
        const data = yield (0, node_fetch_1.default)(URL)
            .then((res) => res.json())
            .then((res) => {
            return res;
        });
        const { email, name, first_name, last_name, picture } = data;
        const password = email + 'your facebook secrect password';
        const passwordHash = yield bcrypt_1.default.hash(password, 12);
        const user = yield user_model_1.default.findOne({ account: email });
        if (user) {
            loginUser(user, password, res);
        }
        else {
            const user = {
                name: first_name + ' ' + last_name,
                account: email,
                password: passwordHash,
                avatar: picture.data.url,
                type: 'facebook',
            };
            registerUser(user, res);
        }
    }
    catch (error) {
        return res.status(500).json('Server error');
    }
});
exports.facebookLogin = facebookLogin;
const registerUser = (user, res) => __awaiter(void 0, void 0, void 0, function* () {
    const newUser = new user_model_1.default(user);
    const access_token = (0, generateToken_1.generateAccessToken)({ id: newUser._id });
    const refresh_token = (0, generateToken_1.generateRefreshToken)({ id: newUser._id }, res);
    newUser.rf_token = refresh_token;
    yield newUser.save();
    res.json({
        msg: 'Login Success!',
        access_token,
        user: Object.assign(Object.assign({}, newUser._doc), { password: '' }),
    });
});
const logout = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    try {
        res.clearCookie('refreshtoken', {
            path: `/api/refresh_token`,
        });
        yield user_model_1.default.findOneAndUpdate({ _id: (_a = req === null || req === void 0 ? void 0 : req.user) === null || _a === void 0 ? void 0 : _a._id }, {
            rf_token: '',
        });
        return res.status(200).json({ msg: 'Logout success !' });
    }
    catch (error) {
        return res.status(500).json('Server error');
    }
});
exports.logout = logout;
const refreshToken = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const rf_token = req.cookies.refreshtoken;
        if (!rf_token)
            return res.status(400).json({ msg: 'Please login now!' });
        const decoded = (jsonwebtoken_1.default.verify(rf_token, `${process.env.REFRESH_TOKEN_SECRET}`));
        if (!decoded.id)
            return res.status(400).json({ msg: 'Please login now!' });
        const user = yield user_model_1.default.findById(decoded.id).select('-password +rf_token');
        if (!user)
            return res.status(400).json({ msg: 'This account does not exist.' });
        // if (rf_token !== user.rf_token)
        //   return res.status(400).json({ msg: 'Please login now!' })
        const access_token = (0, generateToken_1.generateAccessToken)({ id: user._id });
        const refresh_token = (0, generateToken_1.generateRefreshToken)({ id: user._id }, res);
        yield user_model_1.default.findOneAndUpdate({ _id: user._id }, {
            rf_token: refresh_token,
        });
        res.status(200).json({ access_token, user });
    }
    catch (err) {
        return res.status(500).json('Server error');
    }
});
exports.refreshToken = refreshToken;
const loginUser = (user, password, res) => __awaiter(void 0, void 0, void 0, function* () {
    const isMatch = yield bcrypt_1.default.compare(password, user.password);
    if (!isMatch) {
        let msgError = user.type !== 'register'
            ? `Password is incorrect. This account login by ${user.type}`
            : 'Password is incorrect.';
        return res.status(400).json({ msg: msgError });
    }
    const access_token = (0, generateToken_1.generateAccessToken)({ id: user._id });
    const refresh_token = (0, generateToken_1.generateRefreshToken)({ id: user._id }, res);
    yield user_model_1.default.findOneAndUpdate({ _id: user._id }, {
        rf_token: refresh_token,
    });
    return res.status(200).json({
        msg: 'Login successed !',
        access_token,
        user: Object.assign(Object.assign({}, user._doc), { password: '' }),
    });
});
const forgotPassword = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { email } = req.body;
        const user = yield user_model_1.default.findOne({ email });
        if (!user)
            return res.status(400).json({ msg: 'This email does not exist.' });
        if (user.type !== 'register')
            return res.status(400).json({
                msg: `Quick login email with ${user.type} can't use this function.`,
            });
        const access_token = (0, generateToken_1.generateAccessToken)({ id: user._id });
        const url = `${CLIENT_URL}/reset_password/${access_token}`;
        if ((0, valid_1.validateEmail)(email)) {
            (0, serverMail_1.default)(email, url, 'Forgot password?');
            return res.json({ msg: 'Success! Please check your email.' });
        }
    }
    catch (err) {
        return res.status(500).json({ msg: err.message });
    }
});
exports.forgotPassword = forgotPassword;
