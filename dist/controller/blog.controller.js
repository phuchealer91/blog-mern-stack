"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.searchBlogs = exports.deleteBlog = exports.updateBlog = exports.getBlog = exports.getBlogsByUser = exports.getBlogsByCategory = exports.getHomeBlogs = exports.createBlog = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const blog_model_1 = __importDefault(require("../models/blog.model"));
const comment_model_1 = __importDefault(require("../models/comment.model"));
const Pagination = (req) => {
    const page = Number(req.query.page) * 1 || 1;
    const limit = Number(req.query.limit) * 1 || 4;
    const skip = (page - 1) * limit;
    return { page, limit, skip };
};
const createBlog = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    try {
        const { title, content, description, thumbnail, category } = req.body.newBlog;
        const newBlog = new blog_model_1.default({
            user: (_a = req.user) === null || _a === void 0 ? void 0 : _a._id,
            title,
            content,
            description,
            thumbnail,
            category,
        });
        yield newBlog.save();
        return res
            .status(201)
            .json(Object.assign(Object.assign({ msg: 'Create blog successed' }, newBlog._doc), { user: req.user }));
    }
    catch (error) {
        return res.status(500).json({ msg: error.message });
    }
});
exports.createBlog = createBlog;
const getHomeBlogs = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield blog_model_1.default.aggregate([
            {
                $lookup: {
                    from: 'users',
                    let: { userId: '$user' },
                    pipeline: [
                        {
                            // Tìm kiếm kq _id và userId bằng nhau
                            $match: {
                                $expr: {
                                    $eq: ['$_id', '$$userId'],
                                },
                            },
                        },
                    ],
                    as: 'user',
                },
            },
            // array -> object
            { $unwind: '$user' },
            {
                $lookup: {
                    from: 'categories',
                    localField: 'category',
                    foreignField: '_id',
                    as: 'category',
                },
            },
            // array -> object
            { $unwind: '$category' },
            // Sorting
            {
                $sort: { createdAt: -1 },
            },
            // Group by category
            {
                $group: {
                    _id: '$category._id',
                    name: { $first: '$category.name' },
                    slug: { $last: '$category.slug' },
                    blogs: {
                        $push: '$$ROOT',
                    },
                    count: { $sum: 1 },
                },
            },
            // Paginate
            {
                $project: {
                    blogs: {
                        $slice: ['$blogs', 0, 4],
                    },
                    count: 1,
                    name: 1,
                    slug: 1,
                },
            },
        ]);
        return res.status(200).json({ data });
    }
    catch (error) {
        return res.status(500).json({ msg: error.message });
    }
});
exports.getHomeBlogs = getHomeBlogs;
const getBlogsByCategory = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { skip, limit } = Pagination(req);
    try {
        const dataRes = yield blog_model_1.default.aggregate([
            {
                $facet: {
                    totalData: [
                        {
                            $match: {
                                category: new mongoose_1.default.Types.ObjectId(req.params.categoryId),
                            },
                        },
                        {
                            $lookup: {
                                from: 'users',
                                let: { user_id: '$user' },
                                pipeline: [
                                    { $match: { $expr: { $eq: ['$_id', '$$user_id'] } } },
                                    { $project: { password: 0 } },
                                ],
                                as: 'user',
                            },
                        },
                        // array -> object
                        { $unwind: '$user' },
                        // Sorting
                        { $sort: { createdAt: -1 } },
                        { $skip: skip },
                        { $limit: limit },
                    ],
                    totalCount: [
                        {
                            $match: {
                                category: new mongoose_1.default.Types.ObjectId(req.params.categoryId),
                            },
                        },
                        { $count: 'count' },
                    ],
                },
            },
            {
                $project: {
                    count: { $arrayElemAt: ['$totalCount.count', 0] },
                    totalData: 1,
                },
            },
        ]);
        const blogs = dataRes[0].totalData;
        const count = dataRes[0].count;
        let total = 0;
        if (count % limit === 0) {
            total = count / limit;
        }
        else {
            total = Math.floor(count / limit) + 1;
        }
        return res.status(200).json({ blogs, total });
    }
    catch (error) {
        return res.status(500).json({ msg: error.message });
    }
});
exports.getBlogsByCategory = getBlogsByCategory;
const getBlogsByUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { skip, limit } = Pagination(req);
    try {
        const dataRes = yield blog_model_1.default.aggregate([
            {
                $facet: {
                    totalData: [
                        {
                            $match: {
                                user: new mongoose_1.default.Types.ObjectId(req.params.id),
                            },
                        },
                        {
                            $lookup: {
                                from: 'users',
                                let: { user_id: '$user' },
                                pipeline: [
                                    { $match: { $expr: { $eq: ['$_id', '$$user_id'] } } },
                                    { $project: { password: 0 } },
                                ],
                                as: 'user',
                            },
                        },
                        // array -> object
                        { $unwind: '$user' },
                        // Sorting
                        { $sort: { createdAt: -1 } },
                        { $skip: skip },
                        { $limit: limit },
                    ],
                    totalCount: [
                        {
                            $match: {
                                user: new mongoose_1.default.Types.ObjectId(req.params.id),
                            },
                        },
                        { $count: 'count' },
                    ],
                },
            },
            {
                $project: {
                    count: { $arrayElemAt: ['$totalCount.count', 0] },
                    totalData: 1,
                },
            },
        ]);
        const blogs = dataRes[0].totalData;
        const count = dataRes[0].count;
        let total = 0;
        if (count % limit === 0) {
            total = count / limit;
        }
        else {
            total = Math.floor(count / limit) + 1;
        }
        return res.status(200).json({ blogs, total });
    }
    catch (error) {
        return res.status(500).json({ msg: error.message });
    }
});
exports.getBlogsByUser = getBlogsByUser;
const getBlog = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const dataRes = yield blog_model_1.default.findOne({ _id: req.params.id }).populate('user category');
        if (!dataRes)
            return res.status(400).json({ msg: 'Not found' });
        return res.status(200).json({ blog: dataRes });
    }
    catch (error) {
        return res.status(500).json({ msg: error.message });
    }
});
exports.getBlog = getBlog;
const updateBlog = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _b;
    try {
        const updateBlogs = yield blog_model_1.default.findOneAndUpdate({
            _id: req.params.id,
            user: (_b = req.user) === null || _b === void 0 ? void 0 : _b._id,
        }, req.body.updateBlog);
        if (!updateBlogs)
            return res.status(400).json({ msg: 'Not found' });
        return res.status(200).json({ msg: 'Update blog successed', updateBlogs });
    }
    catch (error) {
        return res.status(500).json({ msg: error.message });
    }
});
exports.updateBlog = updateBlog;
const deleteBlog = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _c;
    try {
        // Delete blog
        const deletedBlog = yield blog_model_1.default.findOneAndDelete({
            _id: req.params.id,
            user: (_c = req.user) === null || _c === void 0 ? void 0 : _c._id,
        });
        if (!deletedBlog)
            return res.status(400).json({ msg: 'Not found' });
        // Delete comment
        const deletedAllComments = yield comment_model_1.default.deleteMany({
            blog_id: deletedBlog._id,
        });
        console.log('deletedAllComments', deletedAllComments);
        return res.status(200).json({ msg: 'Delete blog successed' });
    }
    catch (error) {
        return res.status(500).json({ msg: error.message });
    }
});
exports.deleteBlog = deleteBlog;
const searchBlogs = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const blogs = yield blog_model_1.default.aggregate([
            {
                $search: {
                    index: 'searchTitle',
                    autocomplete: {
                        path: 'title',
                        query: `${req.query.title}`,
                    },
                },
            },
            {
                $sort: { createdAt: -1 },
            },
            {
                $limit: 5,
            },
            {
                $project: {
                    title: 1,
                    description: 1,
                    thumbnail: 1,
                    createdAt: 1,
                },
            },
        ]);
        if (!blogs.length)
            return res.status(400).json({ msg: 'No blogs' });
        return res.status(200).json({ blogs });
    }
    catch (error) {
        return res.status(500).json({ msg: error.message });
    }
});
exports.searchBlogs = searchBlogs;
