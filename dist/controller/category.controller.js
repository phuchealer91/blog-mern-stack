"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteCategory = exports.updateCategory = exports.getCategory = exports.createCategory = void 0;
const slugify_1 = __importDefault(require("slugify"));
const category_model_1 = __importDefault(require("../models/category.model"));
const blog_model_1 = __importDefault(require("../models/blog.model"));
const createCategory = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { name } = req.body;
        const slugName = (0, slugify_1.default)(name.toLowerCase());
        const category = yield category_model_1.default.findOne({ slug: slugName });
        if (category)
            return res.status(500).json({ msg: 'Category already exists' });
        const newCategory = new category_model_1.default({
            name: name.toLowerCase(),
            slug: slugName,
        });
        yield newCategory.save();
        return res
            .status(201)
            .json({ msg: 'Create category successed !', category: newCategory });
    }
    catch (error) {
        return res.status(500).json({ msg: error.message });
    }
});
exports.createCategory = createCategory;
const getCategory = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const categories = yield category_model_1.default.find().sort('-createdAt');
        return res.status(200).json({ categories });
    }
    catch (error) {
        return res.status(500).json({ msg: error.message });
    }
});
exports.getCategory = getCategory;
const updateCategory = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { slug } = req.params;
        const { name } = req.body;
        const slugName = (0, slugify_1.default)(name.toLowerCase());
        const category = yield category_model_1.default.findOne({ slug: slug });
        if (!category)
            return res.status(400).json({ msg: 'Category not found' });
        const categoryUpdate = yield category_model_1.default.findOneAndUpdate({
            slug: slug,
        }, { name: name.toLowerCase(), slug: slugName }, { new: true });
        return res
            .status(200)
            .json({ msg: 'Update category successed', categoryUpdate });
    }
    catch (error) {
        return res.status(500).json({ msg: error.message });
    }
});
exports.updateCategory = updateCategory;
const deleteCategory = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { slug } = req.params;
        const category = yield category_model_1.default.findOne({ slug: slug });
        if (!category)
            return res.status(400).json({ msg: 'Category not found' });
        const blogExist = yield blog_model_1.default.findOne({ category: category._id });
        if (blogExist)
            return res
                .status(400)
                .json({
                msg: 'Cannot delete this category. Because It also exist blogs.',
            });
        const categoryDeleted = yield category_model_1.default.findOneAndDelete({
            slug: slug,
        });
        return res
            .status(200)
            .json({ msg: 'Delete category successed', categoryDeleted });
    }
    catch (error) {
        return res.status(500).json({ msg: error.message });
    }
});
exports.deleteCategory = deleteCategory;
