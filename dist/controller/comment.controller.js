"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteComment = exports.updateComment = exports.replyComment = exports.getComments = exports.createComment = void 0;
const comment_model_1 = __importDefault(require("../models/comment.model"));
const mongoose_1 = __importDefault(require("mongoose"));
const Pagination = (req) => {
    const page = Number(req.query.page) * 1 || 1;
    const limit = Number(req.query.limit) * 1 || 4;
    const skip = (page - 1) * limit;
    return { page, limit, skip };
};
const createComment = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    try {
        const { blog_id, blog_user_id, content } = req.body;
        const newComment = new comment_model_1.default({
            user: (_a = req.user) === null || _a === void 0 ? void 0 : _a.id,
            blog_id,
            blog_user_id,
            content,
        });
        yield newComment.save();
        return res
            .status(201)
            .json({ msg: 'Create comment sussuces', comment: newComment });
    }
    catch (error) {
        return res.status(500).json({ msg: error.message });
    }
});
exports.createComment = createComment;
const getComments = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { skip, limit } = Pagination(req);
    try {
        const dataRes = yield comment_model_1.default.aggregate([
            {
                $facet: {
                    totalData: [
                        {
                            $match: {
                                blog_id: new mongoose_1.default.Types.ObjectId(req.params.id),
                                comment_root: { $exists: false },
                                reply_user: { $exists: false },
                            },
                        },
                        {
                            $lookup: {
                                from: 'users',
                                let: { user_id: '$user' },
                                pipeline: [
                                    {
                                        $match: {
                                            $expr: { $eq: ['$_id', '$$user_id'] },
                                        },
                                    },
                                    { $project: { name: 1, avatar: 1 } },
                                ],
                                as: 'user',
                            },
                        },
                        // array -> object
                        { $unwind: '$user' },
                        // Sorting
                        {
                            $lookup: {
                                from: 'comments',
                                let: { cm_id: '$reply_cmt' },
                                pipeline: [
                                    {
                                        $match: {
                                            $expr: { $in: ['$_id', '$$cm_id'] },
                                        },
                                    },
                                    {
                                        $lookup: {
                                            from: 'users',
                                            let: { user_id: '$user' },
                                            pipeline: [
                                                {
                                                    $match: {
                                                        $expr: { $eq: ['$_id', '$$user_id'] },
                                                    },
                                                },
                                                { $project: { name: 1, avatar: 1 } },
                                            ],
                                            as: 'user',
                                        },
                                    },
                                    { $unwind: '$user' },
                                    {
                                        $lookup: {
                                            from: 'users',
                                            let: { user_id: '$reply_user' },
                                            pipeline: [
                                                {
                                                    $match: {
                                                        $expr: { $eq: ['$_id', '$$user_id'] },
                                                    },
                                                },
                                                { $project: { name: 1, avatar: 1 } },
                                            ],
                                            as: 'reply_user',
                                        },
                                    },
                                    { $unwind: '$reply_user' },
                                ],
                                as: 'reply_cmt',
                            },
                        },
                        { $sort: { createdAt: -1 } },
                        { $skip: skip },
                        { $limit: limit },
                    ],
                    totalCount: [
                        {
                            $match: {
                                blog_id: new mongoose_1.default.Types.ObjectId(req.params.id),
                                comment_root: { $exists: false },
                                reply_user: { $exists: false },
                            },
                        },
                        { $count: 'count' },
                    ],
                },
            },
            {
                $project: {
                    count: { $arrayElemAt: ['$totalCount.count', 0] },
                    totalData: 1,
                },
            },
        ]);
        const comments = dataRes[0].totalData;
        const count = dataRes[0].count;
        let total = 0;
        if (count % limit === 0) {
            total = count / limit;
        }
        else {
            total = Math.floor(count / limit) + 1;
        }
        return res.status(200).json({ comments, total });
    }
    catch (error) {
        return res.status(500).json({ msg: error.message });
    }
});
exports.getComments = getComments;
const replyComment = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _b;
    try {
        const { blog_id, blog_user_id, content, reply_user, comment_root } = req.body;
        const newComment = new comment_model_1.default({
            user: (_b = req.user) === null || _b === void 0 ? void 0 : _b.id,
            blog_id,
            blog_user_id,
            content,
            reply_user: reply_user._id,
            comment_root,
        });
        yield comment_model_1.default.findOneAndUpdate({ _id: comment_root }, {
            $push: {
                reply_cmt: newComment._id,
            },
        });
        yield newComment.save();
        return res
            .status(201)
            .json({ msg: 'Create reply comment sussuces', replyComment: newComment });
    }
    catch (error) {
        return res.status(500).json({ msg: error.message });
    }
});
exports.replyComment = replyComment;
const updateComment = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    if (!req.user)
        return res.status(403).json({ msg: 'Invalid Authentication' });
    try {
        const { content } = req.body;
        const comment = yield comment_model_1.default.findOneAndUpdate({ _id: req.params.id, user: req === null || req === void 0 ? void 0 : req.user.id }, { content }, { new: true });
        if (!comment)
            return res.status(400).json({ msg: "Comment doesn't exists" });
        return res.status(200).json({ msg: 'Update success' });
    }
    catch (error) {
        return res.status(500).json({ msg: error.mesage });
    }
});
exports.updateComment = updateComment;
const deleteComment = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    if (!req.user)
        return res.status(403).json({ msg: 'Invalid Authentication' });
    try {
        const { content } = req.body;
        const comment = yield comment_model_1.default.findOneAndDelete({
            _id: req.params.id,
            $or: [{ user: req === null || req === void 0 ? void 0 : req.user.id }, { blog_user_id: req === null || req === void 0 ? void 0 : req.user.id }],
        });
        if (!comment)
            return res.status(400).json({ msg: "Comment doesn't exists" });
        if (comment.comment_root) {
            // update reply
            yield comment_model_1.default.findOneAndUpdate({
                _id: comment.comment_root,
            }, {
                $pull: { reply_cmt: comment._id },
            });
        }
        else {
            // delete all reply
            yield comment_model_1.default.deleteMany({
                _id: {
                    $in: comment.reply_cmt,
                },
            });
        }
        return res.status(200).json({ msg: 'Delete success' });
    }
    catch (error) {
        return res.status(500).json({ msg: error.mesage });
    }
});
exports.deleteComment = deleteComment;
