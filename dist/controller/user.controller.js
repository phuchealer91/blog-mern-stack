"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getUserDetail = exports.updatePassword = exports.updateUser = void 0;
const bcrypt_1 = __importDefault(require("bcrypt"));
const user_model_1 = __importDefault(require("../models/user.model"));
const updateUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    if (!req.user)
        return res.status(400).json({ msg: 'Invalid Authentication' });
    try {
        const { avatar, name } = req.body;
        console.log('avatar', avatar);
        const userUpdated = yield user_model_1.default.findOneAndUpdate({ _id: req.user.id }, {
            avatar: avatar ? avatar : req.user.avatar,
            name: name ? name : req.user.name,
        }, { new: true });
        console.log('userUpdated', userUpdated);
        return res.status(200).json({
            msg: 'Update successed !',
            user: userUpdated,
        });
    }
    catch (error) {
        return res.status(500).json({ msg: 'Server error' });
    }
});
exports.updateUser = updateUser;
const updatePassword = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    if (!req.user)
        return res.status(400).json({ msg: 'Invalid Authentication' });
    if (req.user.type !== 'register') {
        return res.status(400).json({
            msg: `Quick login account with ${req.user.type} can't use this function.`,
        });
    }
    try {
        const { password } = req.body;
        const passwordHash = yield bcrypt_1.default.hash(password, 12);
        yield user_model_1.default.findOneAndUpdate({ _id: req.user.id }, {
            password: passwordHash,
        }, { new: true });
        return res.status(200).json({
            msg: 'Update password successed !',
        });
    }
    catch (error) {
        return res.status(500).json({ msg: 'Server error' });
    }
});
exports.updatePassword = updatePassword;
const getUserDetail = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const userDetail = yield user_model_1.default.findById(req.params.id).select('-password');
        return res.status(200).json({
            user: userDetail,
        });
    }
    catch (error) {
        return res.status(500).json({ msg: 'Server error' });
    }
});
exports.getUserDetail = getUserDetail;
