"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const userSchema = new mongoose_1.default.Schema({
    name: {
        type: String,
        required: [true, 'Please add your name'],
        trim: true,
        maxLength: [20, 'Your name is up to 20 chars long.'],
    },
    account: {
        type: String,
        required: [true, 'Please add your email or phone'],
        trim: true,
        unique: true,
    },
    password: {
        type: String,
        required: [true, 'Please add your password'],
        trim: true,
    },
    avatar: {
        type: String,
        default: 'https://res.cloudinary.com/ecommerce-mp/image/upload/v1655637201/avatar_wljdg7.png',
    },
    role: {
        type: String,
        default: 'user', // admin
    },
    type: {
        type: String,
        default: 'register', // fast
    },
    rf_token: {
        type: String,
        select: false,
    },
}, {
    timestamps: true,
});
exports.default = mongoose_1.default.model('User', userSchema);
