"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const auth_controller_1 = require("./../controller/auth.controller");
const express_1 = __importDefault(require("express"));
const auth_1 = require("../middleware/auth");
const router = express_1.default.Router();
router.post('/register', auth_controller_1.register);
router.post('/active', auth_controller_1.activeAccount);
router.post('/login', auth_controller_1.login);
router.post('/google_login', auth_controller_1.googleLogin);
router.post('/facebook_login', auth_controller_1.facebookLogin);
router.get('/logout', auth_1.auth, auth_controller_1.logout);
router.get('/refresh_token', auth_controller_1.refreshToken);
router.post('/forgot_password', auth_controller_1.forgotPassword);
exports.default = router;
