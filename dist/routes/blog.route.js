"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const blog_controller_1 = require("../controller/blog.controller");
const auth_1 = require("../middleware/auth");
const router = express_1.default.Router();
router.get('/blogs/:categoryId', blog_controller_1.getBlogsByCategory);
router.get('/blogs/user/:id', blog_controller_1.getBlogsByUser);
router
    .route('/blog/:id')
    .get(blog_controller_1.getBlog)
    .put(auth_1.auth, blog_controller_1.updateBlog)
    .delete(auth_1.auth, blog_controller_1.deleteBlog);
router.post('/blog', auth_1.auth, blog_controller_1.createBlog).get('/blogs', blog_controller_1.getHomeBlogs);
router.get('/search/blogs', blog_controller_1.searchBlogs);
exports.default = router;
