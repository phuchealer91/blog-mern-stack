"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const category_controller_1 = require("./../controller/category.controller");
const express_1 = __importDefault(require("express"));
const auth_1 = require("../middleware/auth");
const router = express_1.default.Router();
router.route('/category').post(auth_1.auth, auth_1.isAdmin, category_controller_1.createCategory).get(category_controller_1.getCategory);
router
    .route('/category/:slug')
    .patch(auth_1.auth, auth_1.isAdmin, category_controller_1.updateCategory)
    .delete(auth_1.auth, auth_1.isAdmin, category_controller_1.deleteCategory);
exports.default = router;
