"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const comment_controller_1 = require("./../controller/comment.controller");
const express_1 = __importDefault(require("express"));
const auth_1 = require("../middleware/auth");
const router = express_1.default.Router();
router.post('/comment', auth_1.auth, comment_controller_1.createComment);
router.post('/reply_comment', auth_1.auth, comment_controller_1.replyComment);
router.get('/comment/blog/:id', comment_controller_1.getComments);
router.patch('/comment/:id', auth_1.auth, comment_controller_1.updateComment);
router.delete('/comment/:id', auth_1.auth, comment_controller_1.deleteComment);
exports.default = router;
