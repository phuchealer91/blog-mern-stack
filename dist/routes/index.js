"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const auth_route_1 = __importDefault(require("./auth.route"));
const user_route_1 = __importDefault(require("./user.route"));
const category_route_1 = __importDefault(require("./category.route"));
const blog_route_1 = __importDefault(require("./blog.route"));
const comment_route_1 = __importDefault(require("./comment.route"));
const routers = [
    auth_route_1.default,
    user_route_1.default,
    category_route_1.default,
    blog_route_1.default,
    comment_route_1.default,
];
exports.default = routers;
