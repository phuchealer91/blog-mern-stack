import { ConnectOptions } from 'mongoose'
import mongoose from 'mongoose'

const DB_URL = process.env.MONGOOSE_URL
mongoose.connect(
  `${DB_URL}`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  } as ConnectOptions,
  (err) => {
    if (err) throw err
    console.log('Mongodb connectioned')
  }
)
