import bcrypt from 'bcrypt'
import { Request, Response } from 'express'
import { OAuth2Client } from 'google-auth-library'
import jwt from 'jsonwebtoken'
import fetch from 'node-fetch'
import { IDecodedToken } from '../config/interface'
import sendMail from '../config/serverMail'
import { validateEmail } from '../middleware/valid'
import Users from '../models/user.model'
import {
  generateAccessToken,
  generateActiveToken,
  generateRefreshToken,
} from './../config/generateToken'
import {
  IGgPayload,
  IRequestAuth,
  IUser,
  IUserParams,
} from './../config/interface'
const client = new OAuth2Client(`${process.env.MAIL_CLIENT_ID}`)
const CLIENT_URL = `${process.env.BASE_URL}`

export const register = async (req: Request, res: Response) => {
  try {
    const { name, account, password } = req.body
    const user = await Users.findOne({ account })
    if (user)
      return res
        .status(400)
        .json({ msg: 'Email or Phone number already exists.' })
    const passwordHash = await bcrypt.hash(password, 12)
    const newUser = { name, account, password: passwordHash }
    const active_token = generateActiveToken({ newUser })
    const url = `${CLIENT_URL}/active/${active_token}`
    if (validateEmail(account)) {
      sendMail(account, url, 'Verify your email address')
      return res.status(200).json({ msg: 'Success! Please check your email.' })
    }
    // return res.status(201).json({
    //   status: 'OK',
    //   msg: 'Register successfully.',
    //   data: newUser,
    //   active_token,
    // })
  } catch (error) {
    return res.status(500).json('Server error')
  }
}

export const activeAccount = async (req: Request, res: Response) => {
  try {
    const { active_token } = req.body
    const decoded = <IDecodedToken>(
      jwt.verify(active_token, `${process.env.ACTIVE_TOKEN_SECRET}`)
    )
    const { newUser } = decoded
    if (!newUser) return res.status(400).json({ msg: 'Invalid authentication' })
    const user = await Users.findOne({ account: newUser.account })
    if (user) return res.status(400).json({ msg: 'Account already exists.' })
    const new_user = new Users(newUser)
    await new_user.save()
    return res.status(201).json({ msg: 'Account has been activated!' })
  } catch (error: any) {
    let errMsg = error?.message
    if (error.code === 11000) {
      errMsg = Object.keys(error.keyValue)[0] + 'already exists.'
    }
    return res.status(500).json({ msg: errMsg })
  }
}
export const login = async (req: Request, res: Response) => {
  try {
    const { account, password } = req.body
    const user = await Users.findOne({ account })
    if (!user) return res.status(400).json({ msg: 'Not found' })

    // if user exists
    loginUser(user, password, res)
  } catch (error) {
    return res.status(500).json('Server error')
  }
}
export const googleLogin = async (req: Request, res: Response) => {
  try {
    const { id_token } = req.body
    // console.log('id_token', id_token)
    const verify = await client.verifyIdToken({
      idToken: id_token,
      audience: `${process.env.MAIL_CLIENT_ID}`,
    })
    const { email, email_verified, name, picture } = <IGgPayload>(
      verify.getPayload()
    )
    if (!email_verified)
      return res.status(500).json({ msg: 'Email verification failed.' })
    const password = email + 'your google secrect password'
    const passwordHash = await bcrypt.hash(password, 12)

    const user = await Users.findOne({ account: email })

    if (user) {
      loginUser(user, password, res)
    } else {
      const user = {
        name,
        account: email,
        password: passwordHash,
        avatar: picture,
        type: 'google',
      }
      registerUser(user, res)
    }
  } catch (error) {
    return res.status(500).json('Server error')
  }
}
export const facebookLogin = async (req: Request, res: Response) => {
  try {
    const { accessToken, userID } = req.body
    const URL = `https://graph.facebook.com/v14.0/${userID}/?fields=id,first_name,last_name,middle_name,email,picture&access_token=${accessToken}`
    const data = await fetch(URL)
      .then((res) => res.json())
      .then((res) => {
        return res
      })
    const { email, name, first_name, last_name, picture } = data
    const password = email + 'your facebook secrect password'
    const passwordHash = await bcrypt.hash(password, 12)

    const user = await Users.findOne({ account: email })

    if (user) {
      loginUser(user, password, res)
    } else {
      const user = {
        name: first_name + ' ' + last_name,
        account: email,
        password: passwordHash,
        avatar: picture.data.url,
        type: 'facebook',
      }
      registerUser(user, res)
    }
  } catch (error) {
    return res.status(500).json('Server error')
  }
}
const registerUser = async (user: IUserParams, res: Response) => {
  const newUser = new Users(user)

  const access_token = generateAccessToken({ id: newUser._id })
  const refresh_token = generateRefreshToken({ id: newUser._id }, res)

  newUser.rf_token = refresh_token
  await newUser.save()
  res.json({
    msg: 'Login Success!',
    access_token,
    user: { ...newUser._doc, password: '' },
  })
}

export const logout = async (req: IRequestAuth, res: Response) => {
  try {
    res.clearCookie('refreshtoken', {
      path: `/api/refresh_token`,
    })
    await Users.findOneAndUpdate(
      { _id: req?.user?._id },
      {
        rf_token: '',
      }
    )
    return res.status(200).json({ msg: 'Logout success !' })
  } catch (error) {
    return res.status(500).json('Server error')
  }
}
export const refreshToken = async (req: Request, res: Response) => {
  try {
    const rf_token = req.cookies.refreshtoken
    if (!rf_token) return res.status(400).json({ msg: 'Please login now!' })

    const decoded = <IDecodedToken>(
      jwt.verify(rf_token, `${process.env.REFRESH_TOKEN_SECRET}`)
    )
    if (!decoded.id) return res.status(400).json({ msg: 'Please login now!' })

    const user = await Users.findById(decoded.id).select('-password +rf_token')
    if (!user)
      return res.status(400).json({ msg: 'This account does not exist.' })
    // if (rf_token !== user.rf_token)
    //   return res.status(400).json({ msg: 'Please login now!' })
    const access_token = generateAccessToken({ id: user._id })
    const refresh_token = generateRefreshToken({ id: user._id }, res)
    await Users.findOneAndUpdate(
      { _id: user._id },
      {
        rf_token: refresh_token,
      }
    )

    res.status(200).json({ access_token, user })
  } catch (err) {
    return res.status(500).json('Server error')
  }
}

const loginUser = async (user: IUser, password: string, res: Response) => {
  const isMatch = await bcrypt.compare(password, user.password)
  if (!isMatch) {
    let msgError =
      user.type !== 'register'
        ? `Password is incorrect. This account login by ${user.type}`
        : 'Password is incorrect.'
    return res.status(400).json({ msg: msgError })
  }
  const access_token = generateAccessToken({ id: user._id })
  const refresh_token = generateRefreshToken({ id: user._id }, res)

  await Users.findOneAndUpdate(
    { _id: user._id },
    {
      rf_token: refresh_token,
    }
  )
  return res.status(200).json({
    msg: 'Login successed !',
    access_token,
    user: { ...user._doc, password: '' },
  })
}
export const forgotPassword = async (req: Request, res: Response) => {
  try {
    const { email } = req.body

    const user = await Users.findOne({ email })
    if (!user)
      return res.status(400).json({ msg: 'This email does not exist.' })

    if (user.type !== 'register')
      return res.status(400).json({
        msg: `Quick login email with ${user.type} can't use this function.`,
      })

    const access_token = generateAccessToken({ id: user._id })

    const url = `${CLIENT_URL}/reset_password/${access_token}`

    if (validateEmail(email)) {
      sendMail(email, url, 'Forgot password?')
      return res.json({ msg: 'Success! Please check your email.' })
    }
  } catch (err: any) {
    return res.status(500).json({ msg: err.message })
  }
}
