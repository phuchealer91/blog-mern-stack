import { Response, Request } from 'express'
import mongoose, { Schema, ObjectId } from 'mongoose'
import { IRequestAuth } from '../config/interface'
import Blogs from '../models/blog.model'
import Comments from '../models/comment.model'
const Pagination = (req: IRequestAuth) => {
  const page = Number(req.query.page) * 1 || 1
  const limit = Number(req.query.limit) * 1 || 4
  const skip = (page - 1) * limit
  return { page, limit, skip }
}
export const createBlog = async (req: IRequestAuth, res: Response) => {
  try {
    const { title, content, description, thumbnail, category } =
      req.body.newBlog
    const newBlog = new Blogs({
      user: req.user?._id,
      title,
      content,
      description,
      thumbnail,
      category,
    })
    await newBlog.save()
    return res
      .status(201)
      .json({ msg: 'Create blog successed', ...newBlog._doc, user: req.user })
  } catch (error: any) {
    return res.status(500).json({ msg: error.message })
  }
}
export const getHomeBlogs = async (req: Request, res: Response) => {
  try {
    const data = await Blogs.aggregate([
      {
        $lookup: {
          from: 'users',
          let: { userId: '$user' },
          pipeline: [
            {
              // Tìm kiếm kq _id và userId bằng nhau
              $match: {
                $expr: {
                  $eq: ['$_id', '$$userId'],
                },
              },
            },
          ],

          as: 'user',
        },
      },
      // array -> object
      { $unwind: '$user' },
      {
        $lookup: {
          from: 'categories',
          localField: 'category',
          foreignField: '_id',
          as: 'category',
        },
      },
      // array -> object
      { $unwind: '$category' },
      // Sorting
      {
        $sort: { createdAt: -1 },
      },
      // Group by category
      {
        $group: {
          _id: '$category._id',
          name: { $first: '$category.name' },
          slug: { $last: '$category.slug' },
          blogs: {
            $push: '$$ROOT',
          },
          count: { $sum: 1 },
        },
      },
      // Paginate
      {
        $project: {
          blogs: {
            $slice: ['$blogs', 0, 4],
          },
          count: 1,
          name: 1,
          slug: 1,
        },
      },
    ])
    return res.status(200).json({ data })
  } catch (error: any) {
    return res.status(500).json({ msg: error.message })
  }
}

export const getBlogsByCategory = async (req: Request, res: Response) => {
  const { skip, limit } = Pagination(req)
  try {
    const dataRes = await Blogs.aggregate([
      {
        $facet: {
          totalData: [
            {
              $match: {
                category: new mongoose.Types.ObjectId(req.params.categoryId),
              },
            },
            {
              $lookup: {
                from: 'users',
                let: { user_id: '$user' },
                pipeline: [
                  { $match: { $expr: { $eq: ['$_id', '$$user_id'] } } },
                  { $project: { password: 0 } },
                ],
                as: 'user',
              },
            },
            // array -> object
            { $unwind: '$user' },
            // Sorting
            { $sort: { createdAt: -1 } },
            { $skip: skip },
            { $limit: limit },
          ] as any,
          totalCount: [
            {
              $match: {
                category: new mongoose.Types.ObjectId(req.params.categoryId),
              },
            },
            { $count: 'count' },
          ] as any,
        },
      },
      {
        $project: {
          count: { $arrayElemAt: ['$totalCount.count', 0] },
          totalData: 1,
        },
      },
    ])
    const blogs = dataRes[0].totalData
    const count = dataRes[0].count
    let total = 0
    if (count % limit === 0) {
      total = count / limit
    } else {
      total = Math.floor(count / limit) + 1
    }
    return res.status(200).json({ blogs, total })
  } catch (error: any) {
    return res.status(500).json({ msg: error.message })
  }
}
export const getBlogsByUser = async (req: Request, res: Response) => {
  const { skip, limit } = Pagination(req)
  try {
    const dataRes = await Blogs.aggregate([
      {
        $facet: {
          totalData: [
            {
              $match: {
                user: new mongoose.Types.ObjectId(req.params.id),
              },
            },
            {
              $lookup: {
                from: 'users',
                let: { user_id: '$user' },
                pipeline: [
                  { $match: { $expr: { $eq: ['$_id', '$$user_id'] } } },
                  { $project: { password: 0 } },
                ],
                as: 'user',
              },
            },
            // array -> object
            { $unwind: '$user' },
            // Sorting
            { $sort: { createdAt: -1 } },
            { $skip: skip },
            { $limit: limit },
          ] as any,
          totalCount: [
            {
              $match: {
                user: new mongoose.Types.ObjectId(req.params.id),
              },
            },
            { $count: 'count' },
          ] as any,
        },
      },
      {
        $project: {
          count: { $arrayElemAt: ['$totalCount.count', 0] },
          totalData: 1,
        },
      },
    ])
    const blogs = dataRes[0].totalData
    const count = dataRes[0].count
    let total = 0
    if (count % limit === 0) {
      total = count / limit
    } else {
      total = Math.floor(count / limit) + 1
    }
    return res.status(200).json({ blogs, total })
  } catch (error: any) {
    return res.status(500).json({ msg: error.message })
  }
}

export const getBlog = async (req: Request, res: Response) => {
  try {
    const dataRes = await Blogs.findOne({ _id: req.params.id }).populate(
      'user category'
    )
    if (!dataRes) return res.status(400).json({ msg: 'Not found' })
    return res.status(200).json({ blog: dataRes })
  } catch (error: any) {
    return res.status(500).json({ msg: error.message })
  }
}

export const updateBlog = async (req: IRequestAuth, res: Response) => {
  try {
    const updateBlogs = await Blogs.findOneAndUpdate(
      {
        _id: req.params.id,
        user: req.user?._id,
      },
      req.body.updateBlog
    )
    if (!updateBlogs) return res.status(400).json({ msg: 'Not found' })
    return res.status(200).json({ msg: 'Update blog successed', updateBlogs })
  } catch (error: any) {
    return res.status(500).json({ msg: error.message })
  }
}
export const deleteBlog = async (req: IRequestAuth, res: Response) => {
  try {
    // Delete blog
    const deletedBlog = await Blogs.findOneAndDelete({
      _id: req.params.id,
      user: req.user?._id,
    })
    if (!deletedBlog) return res.status(400).json({ msg: 'Not found' })
    // Delete comment
    const deletedAllComments = await Comments.deleteMany({
      blog_id: deletedBlog._id,
    })
    console.log('deletedAllComments', deletedAllComments)
    return res.status(200).json({ msg: 'Delete blog successed' })
  } catch (error: any) {
    return res.status(500).json({ msg: error.message })
  }
}
export const searchBlogs = async (req: Request, res: Response) => {
  try {
    const blogs = await Blogs.aggregate([
      {
        $search: {
          index: 'searchTitle',
          autocomplete: {
            path: 'title',
            query: `${req.query.title}`,
          },
        },
      },
      {
        $sort: { createdAt: -1 },
      },
      {
        $limit: 5,
      },
      {
        $project: {
          title: 1,
          description: 1,
          thumbnail: 1,
          createdAt: 1,
        },
      },
    ])
    if (!blogs.length) return res.status(400).json({ msg: 'No blogs' })
    return res.status(200).json({ blogs })
  } catch (error: any) {
    return res.status(500).json({ msg: error.message })
  }
}
