import { Request, Response } from 'express'
import slugify from 'slugify'
import Categories from '../models/category.model'
import Blogs from '../models/blog.model'
export const createCategory = async (req: Request, res: Response) => {
  try {
    const { name } = req.body
    const slugName = slugify(name.toLowerCase())
    const category = await Categories.findOne({ slug: slugName })
    if (category)
      return res.status(500).json({ msg: 'Category already exists' })
    const newCategory = new Categories({
      name: name.toLowerCase(),
      slug: slugName,
    })
    await newCategory.save()
    return res
      .status(201)
      .json({ msg: 'Create category successed !', category: newCategory })
  } catch (error: any) {
    return res.status(500).json({ msg: error.message })
  }
}
export const getCategory = async (req: Request, res: Response) => {
  try {
    const categories = await Categories.find().sort('-createdAt')

    return res.status(200).json({ categories })
  } catch (error: any) {
    return res.status(500).json({ msg: error.message })
  }
}
export const updateCategory = async (req: Request, res: Response) => {
  try {
    const { slug } = req.params
    const { name } = req.body
    const slugName = slugify(name.toLowerCase())
    const category = await Categories.findOne({ slug: slug })
    if (!category) return res.status(400).json({ msg: 'Category not found' })
    const categoryUpdate = await Categories.findOneAndUpdate(
      {
        slug: slug,
      },
      { name: name.toLowerCase(), slug: slugName },
      { new: true }
    )
    return res
      .status(200)
      .json({ msg: 'Update category successed', categoryUpdate })
  } catch (error: any) {
    return res.status(500).json({ msg: error.message })
  }
}
export const deleteCategory = async (req: Request, res: Response) => {
  try {
    const { slug } = req.params
    const category = await Categories.findOne({ slug: slug })
    if (!category) return res.status(400).json({ msg: 'Category not found' })
    const blogExist = await Blogs.findOne({ category: category._id })
    if (blogExist)
      return res
        .status(400)
        .json({
          msg: 'Cannot delete this category. Because It also exist blogs.',
        })
    const categoryDeleted = await Categories.findOneAndDelete({
      slug: slug,
    })
    return res
      .status(200)
      .json({ msg: 'Delete category successed', categoryDeleted })
  } catch (error: any) {
    return res.status(500).json({ msg: error.message })
  }
}
