import { IRequestAuth } from './../config/interface'
import Comments from '../models/comment.model'
import { Response, Request } from 'express'
import mongoose from 'mongoose'
const Pagination = (req: IRequestAuth) => {
  const page = Number(req.query.page) * 1 || 1
  const limit = Number(req.query.limit) * 1 || 4
  const skip = (page - 1) * limit
  return { page, limit, skip }
}
export const createComment = async (req: IRequestAuth, res: Response) => {
  try {
    const { blog_id, blog_user_id, content } = req.body
    const newComment = new Comments({
      user: req.user?.id,
      blog_id,
      blog_user_id,
      content,
    })
    await newComment.save()
    return res
      .status(201)
      .json({ msg: 'Create comment sussuces', comment: newComment })
  } catch (error: any) {
    return res.status(500).json({ msg: error.message })
  }
}
export const getComments = async (req: Request, res: Response) => {
  const { skip, limit } = Pagination(req)

  try {
    const dataRes = await Comments.aggregate([
      {
        $facet: {
          totalData: [
            {
              $match: {
                blog_id: new mongoose.Types.ObjectId(req.params.id),
                comment_root: { $exists: false },
                reply_user: { $exists: false },
              },
            },
            {
              $lookup: {
                from: 'users',
                let: { user_id: '$user' },
                pipeline: [
                  {
                    $match: {
                      $expr: { $eq: ['$_id', '$$user_id'] },
                    },
                  },
                  { $project: { name: 1, avatar: 1 } },
                ],
                as: 'user',
              },
            },
            // array -> object
            { $unwind: '$user' },
            // Sorting
            {
              $lookup: {
                from: 'comments',
                let: { cm_id: '$reply_cmt' },
                pipeline: [
                  {
                    $match: {
                      $expr: { $in: ['$_id', '$$cm_id'] },
                    },
                  },
                  {
                    $lookup: {
                      from: 'users',
                      let: { user_id: '$user' },
                      pipeline: [
                        {
                          $match: {
                            $expr: { $eq: ['$_id', '$$user_id'] },
                          },
                        },
                        { $project: { name: 1, avatar: 1 } },
                      ],
                      as: 'user',
                    },
                  },
                  { $unwind: '$user' },
                  {
                    $lookup: {
                      from: 'users',
                      let: { user_id: '$reply_user' },
                      pipeline: [
                        {
                          $match: {
                            $expr: { $eq: ['$_id', '$$user_id'] },
                          },
                        },
                        { $project: { name: 1, avatar: 1 } },
                      ],
                      as: 'reply_user',
                    },
                  },
                  { $unwind: '$reply_user' },
                ],
                as: 'reply_cmt',
              },
            },
            { $sort: { createdAt: -1 } },
            { $skip: skip },
            { $limit: limit },
          ] as any,
          totalCount: [
            {
              $match: {
                blog_id: new mongoose.Types.ObjectId(req.params.id),
                comment_root: { $exists: false },
                reply_user: { $exists: false },
              },
            },
            { $count: 'count' },
          ] as any,
        },
      },
      {
        $project: {
          count: { $arrayElemAt: ['$totalCount.count', 0] },
          totalData: 1,
        },
      },
    ])
    const comments = dataRes[0].totalData
    const count = dataRes[0].count
    let total = 0
    if (count % limit === 0) {
      total = count / limit
    } else {
      total = Math.floor(count / limit) + 1
    }
    return res.status(200).json({ comments, total })
  } catch (error: any) {
    return res.status(500).json({ msg: error.message })
  }
}

export const replyComment = async (req: IRequestAuth, res: Response) => {
  try {
    const { blog_id, blog_user_id, content, reply_user, comment_root } =
      req.body
    const newComment = new Comments({
      user: req.user?.id,
      blog_id,
      blog_user_id,
      content,
      reply_user: reply_user._id,
      comment_root,
    })
    await Comments.findOneAndUpdate(
      { _id: comment_root },
      {
        $push: {
          reply_cmt: newComment._id,
        },
      }
    )
    await newComment.save()
    return res
      .status(201)
      .json({ msg: 'Create reply comment sussuces', replyComment: newComment })
  } catch (error: any) {
    return res.status(500).json({ msg: error.message })
  }
}

export const updateComment = async (req: IRequestAuth, res: Response) => {
  if (!req.user) return res.status(403).json({ msg: 'Invalid Authentication' })
  try {
    const { content } = req.body
    const comment = await Comments.findOneAndUpdate(
      { _id: req.params.id, user: req?.user.id },
      { content },
      { new: true }
    )
    if (!comment) return res.status(400).json({ msg: "Comment doesn't exists" })
    return res.status(200).json({ msg: 'Update success' })
  } catch (error: any) {
    return res.status(500).json({ msg: error.mesage })
  }
}
export const deleteComment = async (req: IRequestAuth, res: Response) => {
  if (!req.user) return res.status(403).json({ msg: 'Invalid Authentication' })
  try {
    const { content } = req.body
    const comment = await Comments.findOneAndDelete({
      _id: req.params.id,
      $or: [{ user: req?.user.id }, { blog_user_id: req?.user.id }],
    })
    if (!comment) return res.status(400).json({ msg: "Comment doesn't exists" })
    if (comment.comment_root) {
      // update reply
      await Comments.findOneAndUpdate(
        {
          _id: comment.comment_root,
        },
        {
          $pull: { reply_cmt: comment._id },
        }
      )
    } else {
      // delete all reply
      await Comments.deleteMany({
        _id: {
          $in: comment.reply_cmt,
        },
      })
    }
    return res.status(200).json({ msg: 'Delete success' })
  } catch (error: any) {
    return res.status(500).json({ msg: error.mesage })
  }
}
