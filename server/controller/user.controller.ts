import { IRequestAuth } from './../config/interface'
import { Response, Request } from 'express'
import bcrypt from 'bcrypt'
import Users from '../models/user.model'
export const updateUser = async (req: IRequestAuth, res: Response) => {
  if (!req.user) return res.status(400).json({ msg: 'Invalid Authentication' })
  try {
    const { avatar, name } = req.body
    console.log('avatar', avatar)
    const userUpdated = await Users.findOneAndUpdate(
      { _id: req.user.id },
      {
        avatar: avatar ? avatar : req.user.avatar,
        name: name ? name : req.user.name,
      },
      { new: true }
    )
    console.log('userUpdated', userUpdated)
    return res.status(200).json({
      msg: 'Update successed !',
      user: userUpdated,
    })
  } catch (error) {
    return res.status(500).json({ msg: 'Server error' })
  }
}

export const updatePassword = async (req: IRequestAuth, res: Response) => {
  if (!req.user) return res.status(400).json({ msg: 'Invalid Authentication' })
  if (req.user.type !== 'register') {
    return res.status(400).json({
      msg: `Quick login account with ${req.user.type} can't use this function.`,
    })
  }
  try {
    const { password } = req.body
    const passwordHash = await bcrypt.hash(password, 12)
    await Users.findOneAndUpdate(
      { _id: req.user.id },
      {
        password: passwordHash,
      },
      { new: true }
    )
    return res.status(200).json({
      msg: 'Update password successed !',
    })
  } catch (error) {
    return res.status(500).json({ msg: 'Server error' })
  }
}

export const getUserDetail = async (req: Request, res: Response) => {
  try {
    const userDetail = await Users.findById(req.params.id).select('-password')
    return res.status(200).json({
      user: userDetail,
    })
  } catch (error) {
    return res.status(500).json({ msg: 'Server error' })
  }
}
