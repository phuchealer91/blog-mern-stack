import dotenv from 'dotenv'
dotenv.config()
import express from 'express'
import cors from 'cors'
import morgan from 'morgan'
import cookieParser from 'cookie-parser'
import routers from './routes'
import path from 'path'

// MiddleWare
const app = express()
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(morgan('dev'))
app.use(cors({ origin: true, credentials: true }))
app.use(cookieParser())

app.use('/api', routers)

// Database
import './config/db'
// Deploy production
if (process.env.NODE_ENV === 'production') {
  app.use(express.static('client/build'))
  app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../client', 'build', 'index.html'))
  })
}
// Listening
const PORT = process.env.PORT || 5500
app.listen(PORT, () => {
  console.log('Server is running on port', PORT)
})
