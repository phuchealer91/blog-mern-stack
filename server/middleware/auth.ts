import { NextFunction, Response } from 'express'
import jwt from 'jsonwebtoken'
import Users from '../models/user.model'
import { IDecodedToken, IRequestAuth } from './../config/interface'
export const auth = async (
  req: IRequestAuth,
  res: Response,
  next: NextFunction
) => {
  try {
    const token = req.headers['authorization']
    if (!token) return res.status(400).json({ msg: 'Invalid Authentication' })
    console.log('token', token)
    const decoded = <IDecodedToken>(
      jwt.verify(token, `${process.env.ACCESS_TOKEN_SECRET}`)
    )
    if (!decoded) return res.status(400).json({ msg: 'Invalid Authentication' })
    const user = await Users.findOne({ _id: decoded.id })
    if (!user) return res.status(400).json({ msg: 'User does not exits' })
    req.user = user
    next()
  } catch (error) {
    return res.status(500).json({ msg: 'Server error' })
  }
}
export const isAdmin = async (
  req: IRequestAuth,
  res: Response,
  next: NextFunction
) => {
  try {
    const user = await Users.findOne({ account: req.user?.account })
    if (!user) return res.status(400).json({ msg: 'Not found' })
    if (user?.role === 'admin') {
      next()
    } else {
      return res.status(403).json({ msg: 'Admin resource. Access denied.' })
    }
  } catch (error) {
    return res.status(500).json({ msg: 'Server error' })
  }
}
