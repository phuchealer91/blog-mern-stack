import { ICategory } from './../config/interface'
import mongoose from 'mongoose'
const categorySchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      unique: true,
    },
    slug: {
      type: String,
      unique: true,
    },
  },
  {
    timestamps: true,
  }
)

export default mongoose.model<ICategory>('Category', categorySchema)
