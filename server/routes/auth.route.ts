import {
  register,
  activeAccount,
  login,
  logout,
  refreshToken,
  googleLogin,
  facebookLogin,
  forgotPassword,
} from './../controller/auth.controller'
import express from 'express'
import { auth } from '../middleware/auth'

const router = express.Router()
router.post('/register', register)
router.post('/active', activeAccount)
router.post('/login', login)
router.post('/google_login', googleLogin)
router.post('/facebook_login', facebookLogin)
router.get('/logout', auth, logout)
router.get('/refresh_token', refreshToken)
router.post('/forgot_password', forgotPassword)

export default router
