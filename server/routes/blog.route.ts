import express from 'express'
import {
  createBlog,
  deleteBlog,
  getBlog,
  getBlogsByCategory,
  getBlogsByUser,
  getHomeBlogs,
  updateBlog,
  searchBlogs,
} from '../controller/blog.controller'
import { auth } from '../middleware/auth'
const router = express.Router()
router.get('/blogs/:categoryId', getBlogsByCategory)
router.get('/blogs/user/:id', getBlogsByUser)
router
  .route('/blog/:id')
  .get(getBlog)
  .put(auth, updateBlog)
  .delete(auth, deleteBlog)
router.post('/blog', auth, createBlog).get('/blogs', getHomeBlogs)
router.get('/search/blogs', searchBlogs)
export default router
