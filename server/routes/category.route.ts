import {
  createCategory,
  getCategory,
  updateCategory,
  deleteCategory,
} from './../controller/category.controller'
import express from 'express'
import { auth, isAdmin } from '../middleware/auth'

const router = express.Router()
router.route('/category').post(auth, isAdmin, createCategory).get(getCategory)

router
  .route('/category/:slug')
  .patch(auth, isAdmin, updateCategory)
  .delete(auth, isAdmin, deleteCategory)

export default router
