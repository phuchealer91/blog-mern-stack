import {
  createComment,
  getComments,
  replyComment,
  updateComment,
  deleteComment,
} from './../controller/comment.controller'
import express from 'express'
import { auth } from '../middleware/auth'
const router = express.Router()
router.post('/comment', auth, createComment)
router.post('/reply_comment', auth, replyComment)
router.get('/comment/blog/:id', getComments)
router.patch('/comment/:id', auth, updateComment)
router.delete('/comment/:id', auth, deleteComment)

export default router
