import authRouter from './auth.route'
import userRouter from './user.route'
import categoryRouter from './category.route'
import blogRouter from './blog.route'
import commentRouter from './comment.route'

const routers = [
  authRouter,
  userRouter,
  categoryRouter,
  blogRouter,
  commentRouter,
]
  

export default routers
