import express from 'express'
import {
  updateUser,
  updatePassword,
  getUserDetail,
} from '../controller/user.controller'
import { auth } from '../middleware/auth'

const router = express.Router()
router.patch('/user', auth, updateUser)
router.patch('/reset_password', auth, updatePassword)
router.get('/user/:id', getUserDetail)

export default router
